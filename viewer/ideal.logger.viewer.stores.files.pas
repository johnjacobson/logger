unit ideal.logger.viewer.stores.files;

interface

uses
  System.Classes,
  System.SysUtils,
  System.Generics.Defaults,
  System.Generics.Collections,
  cocinasync.flux.store,
  cocinasync.Async,
  ideal.logger.viewer.types,
  ideal.logger.viewer.actions;

type
  TFilesStore = class(TBaseStore)
  strict private
    class var FData : TFilesStore;
  strict private
    FAppFiles: TDictionary<string, TList<TFileRecord>>;
    FFiles: TList<TFileRecord>;
    FDownloads : TList<TFileRecord>;
    FPersistFilename : string;
    FAsync : TAsync;
    FComparer : TFileComparer;
    procedure RestoreFileList;
    procedure PersistFileList;
    procedure OnSelectedFile(Sender : TObject; Action : TOpenLogFileAction);
    procedure OnCloseFile(Sender : TObject; Action : TCloseLogFileAction);
    procedure OnDeleteFile(Sender : TObject; Action : TDeleteLogFileAction);
    procedure StartFileMonitor;
  public
    constructor Create; override;
    destructor Destroy; override;

    class property Data : TFilesStore read FData;
    class constructor Create;
    class destructor Destroy;


    property Files : TList<TFileRecord> read FFiles;
    property AppFiles : TDictionary<string, TList<TFileRecord>> read FAppFiles;
    property Downloads : TList<TFileRecord> read FDownloads;
  end;

implementation

uses
  System.IOUtils,
  System.ZIp,
  System.DateUtils,
  chimera.json,
  cocinasync.jobs,
  cocinasync.flux.dispatcher;

const
  PARALLELS_DOWNLOADS_FOLDER = '\\Mac\\Home\Downloads\';
  PARALLELS_DOCUMENTS_FOLDER = '\\Mac\\Home\Documents\';

{ TFilesStore }

class constructor TFilesStore.Create;
begin
  FData := TFilesStore.Create;
end;

destructor TFilesStore.Destroy;
begin
  FAsync.Free;
  FFiles.Free;
  FDownloads.Free;
  FAppFiles.Free;

  Flux.Unregister<TOpenLogFileAction>(Self);
  Flux.Unregister<TCloseLogFileAction>(Self);
  Flux.Unregister<TDeleteLogFileAction>(Self);

  FComparer.Free;
  inherited;
end;

constructor TFilesStore.Create;
begin
  inherited;
  FComparer := TFileComparer.Create;
  FAsync := TAsync.Create;
  FAppFiles := TDictionary<string, TList<TFileRecord>>.Create;
  FFiles := TList<TFileRecord>.Create;
  FDownloads := TList<TFileRecord>.Create;
  FPersistFilename := TPath.Combine(TPath.GetCachePath, 'ideal.logger.vieweer.json');

  Flux.Register<TOpenLogFileAction>(Self,OnSelectedFile);
  Flux.Register<TCloseLogFileAction>(Self, OnCloseFile);
  Flux.Register<TDeleteLogFileAction>(Self, OnDeleteFile);

  RestoreFileList;

  StartFileMonitor;
end;

class destructor TFilesStore.Destroy;
begin
  FData.Free;
end;

procedure TFilesStore.OnCloseFile(Sender: TObject;
  Action: TCloseLogFileAction);
begin
  for var i := FFiles.Count-1 downto 0 do
  begin
    if TPath.Combine(FFiles[1].Path, FFiles[i].FileName) = Action.FilePath then
      FFiles.Delete(i);
  end;
  PersistFileList;
end;

procedure TFilesStore.OnDeleteFile(Sender: TObject;
  Action: TDeleteLogFileAction);
begin
  for var i := FFiles.Count-1 downto 0 do
  begin
    if TPath.Combine(FFiles[1].Path, FFiles[i].FileName) = Action.FilePath then
      FFiles.Delete(i);
  end;
  TFile.Delete(Action.FilePath);
  PersistFileList;
end;

procedure TFilesStore.OnSelectedFile(Sender: TObject;
  Action: TOpenLogFileAction);
begin
  FFiles.Add(TFileRecord.From(Action.Filepath, ''));
  PersistFileList;
end;

procedure TFilesStore.PersistFileList;
var
  jso : IJSONObject;
  jsa : IJSONArray;
  i : integer;
begin
  jsa := TJSONArray.New;
  for i := 0 to FFiles.Count-1 do
    jsa.Add(FFiles[i].FilePath);
  if TFile.Exists(FPersistFilename) then
    jso := TJSON.FromFile(FPersistFilename)
  else
    jso := TJSON.New;
  jso.Arrays['files'] := jsa;
  jso.SaveToFile(FPersistFilename);
end;

procedure TFilesStore.RestoreFileList;
var
  jso : IJSONObject;
begin
  FFiles.Clear;
  if TFile.Exists(FPersistFilename) then
  begin
    jso := TJSON.FromFile(FPersistFilename);
    jso.Arrays['files'].Each(
      procedure(const filename : string)
      begin
        //FFiles.Add(TFilesStore.TFileRecord.From(filename, ''));
      end
    );
    UpdateViews;
  end;
end;

procedure TFilesStore.StartFileMonitor;
var
  proc : TProc;
begin
  proc := procedure
    function AddDownloadsFrom(Path : String) : boolean;
    var
      ary : TArray<string>;
      sFile : string;
    begin
      Result := False;
      if TDirectory.Exists(Path) then
      begin
        ary := TDirectory.GetFiles(Path, '*.json*', TSearchOption.soAllDirectories);
        for sFile in ary do
        begin
          var fi := TFileRecord.From(sFile, Path);
          if TFileRecord.IsValidFilename(fi.FileName) then
            if not fi.IsInList(FDownloads) then
            begin
              FDownloads.Add(fi);
              Result := True;
            end;
        end;
      end;
    end;
    var
      ary : TArray<String>;
      sPath: string;
      sRoot : string;
      sFile: string;
      bChanged : boolean;
    begin
      bChanged := False;
      sRoot :=
        TPath.GetHomePath+TPath.DirectorySeparatorChar+
        'ideal'+TPath.DirectorySeparatorChar+
        'logs'+TPath.DirectorySeparatorChar;
      if TDirectory.Exists(sRoot) then
      begin
        ary := TDirectory.GetDirectories(sRoot);
        for sPath in ary do
        begin
          var sFolder := sPath.Replace(sRoot,'');
          if not FAppFiles.ContainsKey(sFolder) then
          begin
            FAppFiles.Add(sFolder, TList<TFileRecord>.Create);
            bChanged := True;
          end;
          ary := TDirectory.GetFiles(sPath, '*.json*');
          for sFile in ary do
          begin
            var fi := TFileRecord.From(sFile, sRoot);
            if TFileRecord.isValidFilename(fi.Filename) then
              if not fi.IsInList(FAppFiles[sFolder]) then
              begin
                FAppFiles[sFolder].Add(fi);
                bChanged := True;
              end;
          end;
        end;
      end;

      if TDirectory.Exists(PARALLELS_DOWNLOADS_FOLDER) then
        bChanged := AddDownloadsFrom(PARALLELS_DOWNLOADS_FOLDER) or bChanged;
      if TDirectory.Exists(TPath.GetDownloadsPath) then
        bChanged := AddDownloadsFrom(TPath.GetDownloadsPath) or bChanged;
      if TDirectory.Exists(TPath.GetSharedDownloadsPath) then
        bChanged := AddDownloadsFrom(TPath.GetSharedDownloadsPath) or bChanged;

      if bChanged then
        TAsync.SynchronizeIfInThread(
          procedure
          begin
            for var p in FAppFiles.ToArray do
            begin
              p.Value.Sort(FComparer);
            end;
            FFiles.Sort(FComparer);
            FDownloads.Sort(FComparer);
            UpdateViews;
          end
        );
    end;

  proc();
  FAsync.DoEvery(
  10000,
    proc,
    False
  );
end;

end.
