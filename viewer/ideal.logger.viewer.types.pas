unit ideal.logger.viewer.types;

interface

uses
  System.SysUtils,
  System.Classes,
  System.Generics.Defaults,
  System.Generics.Collections;

type
  TLineInfo = record
    LineNumber : UInt64;
    Msg : String;
    Thread : Int64;
    Level : String;
    Delta : Int64;
    Duration : Double;
    StartPOS : Integer;
    Length : Integer;
    TimeStamp : TDateTime;
    class function From(AStream : TStream; AStartPOS, ALength : Integer) : TLineInfo; static;
  end;

  ILineGroupInfo = interface
    function GetLineFrom : UInt64;
    function GetLineTo : UInt64;
    function GetDuration : Double;
    function GetText : string;
    function GetNotes : string;
    function GetDelta : Int64;
    function GetItems : TList<ILineGroupInfo>;
    function GetTimestamp : TDateTime;
    function GetThread : Int64;
    function GetLevel : String;

    property LineFrom : UInt64 read GetLineFrom;
    property LineTo : UInt64 read GetLineTo;
    property Duration : Double read GetDuration;
    property Notes : string read GetNotes;
    property Text : string read GetText;
    property Items : TList<ILineGroupInfo> read GetItems;
    property Delta : Int64 read GetDelta;
    property Timestamp : TDateTime read GetTimestamp;
    property Thread : Int64 read GetThread;
    property Level : String read GetLevel;
  end;

  TFileRecord = record
    FileName : string;
    FilenameDate : TDateTIme;
    Path : string;
    Root : string;
    Size : UInt64;
    class operator Equal(const Val1, Val2 : TFileRecord) : boolean;
    class operator NotEqual(const Val1, Val2 : TFileRecord) : boolean;
    class operator GreaterThan(const Val1, Val2 : TFileRecord) : boolean;
    class operator LessThan(const Val1, Val2 : TFileRecord) : boolean;
    class operator GreaterThanOrEqual(const Val1, Val2 : TFileRecord) : boolean;
    class operator LessThanOrEqual(const Val1, Val2 : TFileRecord) : boolean;
    class function From(const AFilepath, ARoot : string) : TFileRecord; static;
    function FilePath : string;
    function RelativeFilePath : string;
    function IsInList(List : TList<TFileRecord>) : boolean;
    class function IsValidFilename(Filename : string) : boolean; static;
  end;
  TFileComparer = class(TInterfacedObject, IComparer<TFileRecord>)
  strict private
    function Compare(const Left, Right: TFileRecord): Integer;
  end;

implementation

uses
  System.IOUtils,
  System.DateUtils,
  chimera.json;

{ TLineInfo }

class function TLineInfo.From(AStream : TStream; AStartPOS, ALength: Integer): TLineInfo;
var
  ary : TArray<byte>;
begin
  Result.StartPOS := AStartPOS;
  Result.Length := ALength;
  var iPos := AStream.Position;
  SetLength(ary, ALength);

  AStream.Position := AStartPOS;
  AStream.Read(ary, ALength);
  AStream.Position := iPos;

  var jso := TJSON.From(TEncoding.UTF8.GetString(ary));
  Result.LineNumber := jso.Integers['@i'];
  Result.Msg := jso.Strings['@m'];
  Result.Level := jso.Strings['@l'];
  Result.Delta := jso.Integers['@ms_delta'];
  Result.Thread := jso.Integers['@thread_id'];
  Result.TimeStamp := jso.Dates['@t'];
  if jso.Has['@method_duration'] and (jso.Types['@method_duration'] = TJSONValueType.number) then
    Result.Duration := jso.Numbers['@method_duration']
  else
    Result.Duration := 0;
end;

{ TFileRecord }

class operator TFileRecord.Equal(const Val1,
  Val2: TFileRecord): boolean;
begin
  Result := Val1.FilePath = Val2.FilePath;
end;

function TFileRecord.FilePath: string;
begin
  Result := TPath.Combine(Path, Filename);
end;

class function TFileRecord.From(const AFilepath, ARoot: string): TFileRecord;
  function ExtractFileDate(const Filename : string) : TDateTime;
  begin
    Result := 0;
    if not IsValidFilename(Filename) then
      exit;
    var ary := Filename.Split(['.']);
    var aryDT := ary[Length(ary)-5].Split([' ']);
    var aryDate := aryDT[0].Split(['-']);
    if length(aryDate) <> 3 then
      exit;
    Result := EncodeDateTime(aryDate[0].ToInteger, aryDate[1].ToInteger, aryDate[2].ToInteger, aryDT[1].ToInteger, ary[Length(ary)-4].ToInteger, ary[Length(ary)-3].ToInteger, ary[Length(ary)-2].ToInteger);
  end;
begin
  Result.FileName := ExtractFilename(AFilepath);
  Result.Path := ExtractFilepath(AFilepath);
  Result.Root := ARoot;
  Result.FilenameDate := ExtractFileDate(Result.Filename);

  var fs := TFile.OpenRead(AFilePath);
  try
    if ExtractFileExt(AFilePath).ToLower = '.jsonz' then
    begin
      fs.Position := 4;
      fs.Read(Result.Size,4);
    end else
      Result.Size := fs.Size;
  finally
    fs.Free;
  end;

end;

class operator TFileRecord.GreaterThan(const Val1,
  Val2: TFileRecord): boolean;
begin
  Result := Val1.FilePath > Val2.FilePath;
end;

class operator TFileRecord.GreaterThanOrEqual(const Val1,
  Val2: TFileRecord): boolean;
begin
  Result := Val1.FilePath >= Val2.FilePath;
end;

function TFileRecord.IsInList(List: TList<TFileRecord>): boolean;
begin
  result := False;
  for var fi in List do
  begin
    if fi = Self then
      Exit(True);
  end;

end;

class function TFileRecord.IsValidFilename(
  Filename: string): boolean;
var
  ary : TArray<String>;
begin
  Result := Filename.EndsWith('.json') or Filename.EndsWith('.jsonz');
  if Result then
  begin
    ary := Filename.Split(['.']);
    Result := Length(ary) >= 6;
    if Result then
    begin
      Result := (Length(ary[Length(ary)-5]) = 13) and
                (Length(ary[Length(ary)-4]) = 2) and
                (Length(ary[Length(ary)-3]) = 2) and
                (Length(ary[Length(ary)-2]) = 3);

    end;

  end;
end;

class operator TFileRecord.LessThan(const Val1,
  Val2: TFileRecord): boolean;
begin
  Result := Val1.FilePath < Val2.FilePath;
end;

class operator TFileRecord.LessThanOrEqual(const Val1,
  Val2: TFileRecord): boolean;
begin
  Result := Val1.FilePath <= Val2.FilePath;
end;

class operator TFileRecord.NotEqual(const Val1,
  Val2: TFileRecord): boolean;
begin
  Result := Val1.FilePath <> Val2.FilePath;
end;

function TFileRecord.RelativeFilePath: string;
begin
  Result := FilePath.Replace(Root,'');
end;

{ TFileComparer }

function TFileComparer.Compare(const Left,
  Right: TFileRecord): Integer;
begin
  Result := -1 * CompareText(Left.Filename, Right.FileName);
end;

end.
