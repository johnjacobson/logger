unit ideal.logger.viewer.forms.main;

interface

uses
  System.SysUtils,
  System.Types,
  System.UITypes,
  System.Classes,
  System.Variants,
  System.Generics.Collections,
  System.DateUtils,
  ideal.logger.viewer.stores.files,
  ideal.logger.viewer.stores.open,
  ideal.logger.viewer.types,
  ideal.logger.viewer.tree,
  FMX.Types,
  FMX.Controls,
  FMX.Forms,
  FMX.Graphics,
  FMX.Dialogs,
  FMX.Objects,
  FMX.Layouts,
  FMX.TreeView,
  FMX.Ani,
  FMX.StdCtrls,
  FMX.Effects,
  FMX.TabControl,
  FMX.Memo.Types,
  FMX.Controls.Presentation,
  FMX.ScrollBox,
  FMX.Memo;

type
  TFileTreeViewItem = class(TTreeViewItem)
  private
    FFileSize : TText;
    FItem: TFileRecord;
    procedure SetItem(const Value: TFileRecord);
  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;
    property Item : TFileRecord read FItem write SetItem;
  end;

{  TEntryTreeViewItem = class(TTreeViewItem)
  private
    FItem : ILineGroupInfo;
    FDuration : TText;
    FDelta : TText;
    FTimestamp : TText;
    FLevel : TText;
    FID : TText;
    FMsg : TText;
    FBack : TRectangle;
    procedure SetItem(const Value: ILineGroupInfo);
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property Item : ILineGroupInfo read FItem write SetItem;

  end;         }

  TfrmMain = class(TForm)
    rectHeader: TRectangle;
    txtMenu: TText;
    tvFiles: TTreeView;
    tviApplications: TTreeViewItem;
    tviDownloads: TTreeViewItem;
    tviOther: TTreeViewItem;
    txtOpen: TText;
    dlgOpen: TOpenDialog;
    splLeft: TSplitter;
    seMenu: TShadowEffect;
    seOpen: TShadowEffect;
    txtCaption: TText;
    txtDetails: TMemo;
    splRight: TSplitter;
    txtWarnings: TText;
    txtErrors: TText;
    txtTraceDebug: TText;
    txtInfo: TText;
    seErrors: TShadowEffect;
    seTraceDebug: TShadowEffect;
    seInfo: TShadowEffect;
    seWarnings: TShadowEffect;
    loContent: TLayout;
    loButtons: TLayout;
    rrErrors: TRoundRect;
    rrWarnings: TRoundRect;
    rrTraceDebug: TRoundRect;
    rrInfo: TRoundRect;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure txtMenuClick(Sender: TObject);
    procedure txtOpenClick(Sender: TObject);
    procedure tvFilesChange(Sender: TObject);
    procedure txtCaptionClick(Sender: TObject);
    procedure txtTraceDebugClick(Sender: TObject);
    procedure txtInfoClick(Sender: TObject);
    procedure txtErrorsClick(Sender: TObject);
    procedure txtWarningsClick(Sender: TObject);
  strict private
    FShowTraceDebug : boolean;
    FShowInfo : boolean;
    FShowWarnings : boolean;
    FShowErrors : boolean;
    FFilesWidth : Single;
    FCurrentThread: UInt64;
    FSelectedFile : TFileRecord;
    FOpenFile : TLogTree;
    procedure OnScroll(Sender : TObject);
    procedure SelectThread(Sender : TObject);
    procedure RebuildTree;
    procedure FillFromParent(tviParent : TTreeViewItem; Files : TList<TFileRecord>);
    procedure UpdateFiles(Files : TList<TFileRecord>);
    procedure UpdateAppFiles(Files : TDictionary<string, TList<TFileRecord>>);
    procedure UpdateDownloads(Files : TList<TFileRecord>);
    procedure SetSelectedFile(const Value: TFilerecord);
    procedure SetCurrentThread(const Value: UInt64);
    procedure OpenFileChildCountNeeded(Sender : TObject; Path : TArray<Cardinal>; out LineNumber, Count : Cardinal);
    procedure OpenFileTextNeeded(Sender : TObject; Path : TArray<Cardinal>; var LineNumber : Cardinal; var Text : TArray<String>; var BackgroundColor : TAlphaColor; var ChildCount : Cardinal);
    procedure SelectItem(Sender : TObject; Path : TArray<Cardinal>);
    procedure ExpandItem(Sender : TObject; Path : TArray<Cardinal>; Expanded : boolean);
    function PathToItem(ThreadID : UInt64; Path : TArray<Cardinal>) : ILineGroupInfo;

    property SelectedFile : TFilerecord read FSelectedFile write SetSelectedFile;
    property CurrentThread : UInt64 read FCurrentThread write SetCurrentThread;
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

uses
  cocinasync.flux.fmx.actions.ui,
  ideal.logger.viewer.actions;

{$R *.fmx}

procedure TfrmMain.ExpandItem(Sender: TObject; Path: TArray<Cardinal>;
  Expanded: boolean);
begin
  Self.Invalidate;
end;

procedure TfrmMain.FillFromParent(tviParent: TTreeViewItem;
  Files: TList<TFileRecord>);
var
  i, j: Integer;
begin
  tviParent.BeginUpdate;
  try
    i := 0;
    j := 0;
    while j < Files.Count-1 do
    begin
      if i < tviParent.Count-1 then
      begin
        tviParent.items[i].Text := Files[j].FileName;
      end else
      begin
        var tvi := TFileTreeViewItem.Create(tvFiles);
        tvi.Parent := tviParent;
        tvi.Item := Files[j];
      end;
      inc(i);
      inc(j);
    end;
    while i < tviParent.Count do
    begin
      var tvi := tviParent.Items[i];
      tvi.Parent := nil;
      tvi.Free;
    end;
  finally
    tviParent.EndUpdate;
  end;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
var
  ary : TArray<Cardinal>;
begin
  FFilesWidth := tvFiles.Width;

  FOpenFile := TLogTree.Create(Self);
  FOpenFile.Parent := loContent;
  FOpenFile.Align := TAlignLayout.Client;
  FOpenFile.OnChildCountNeeded := OpenFileChildCountNeeded;
  FOpenFile.OnTextNeeded := OpenFileTextNeeded;
  FOpenFile.OnSelectItem := SelectItem;
  FOpenFile.OnExpandItemEvent := ExpandItem;
  FOpenFile.OnScroll := OnScroll;

  SetLength(ary, 3);
  ary[0] := 75;
  ary[1] := 75;
  ary[2] := 75;
  FOpenFile.LeftCols := ary;
  SetLength(ary,2);
  ary[0] := 75;
  ary[1] := 175;
  FOpenFile.RightCols := ary;

  FShowTraceDebug := True;
  FShowInfo := True;
  FShowWarnings := True;
  FShowErrors := True;

  TFilesStore.Data.RegisterForUpdates<TFilesStore>(Self,
    procedure(Store : TFilesStore)
    begin
      tvFiles.BeginUpdate;
      try
        UpdateFiles(Store.Files);
        UpdateAppFiles(Store.AppFiles);
        UpdateDownloads(Store.Downloads);
      finally
        tvFiles.EndUpdate;
      end;
      Self.Invalidate;
    end
  );
  TOpenStore.Data.RegisterForUpdates<TOpenStore>(Self,
    procedure(Store : TOpenStore)
    var
      btn : TButton;
    begin
      loButtons.BeginUpdate;
      try
        for var i := loButtons.ControlsCount-1 downto 0 do
        begin
          btn := TButton(loButtons.Controls[i]);
          btn.align := TAlignLayout.none;
          btn.Position.y := 100;
          btn.Free;
        end;

        for var i := 0 to Store.Threads.Count-1 do
        begin
          btn := TButton.Create(loButtons);
          btn.Parent := loButtons;
          btn.OnClick := SelectThread;
          btn.StaysPressed := True;
          btn.Align := TAlignLayout.Left;
          btn.Margins.Left := 4.000000000000000000;
          btn.Margins.Top := 4.000000000000000000;
          btn.Size.Width := 80.000000000000000000;
          btn.Size.Height := 33.000000000000000000;
          btn.Size.PlatformDefault := False;
          btn.TabOrder := 0;
          btn.Text := 'Thread '+Store.Threads[i].Key.ToString;
          btn.Tag := Store.Threads[i].Key;
          btn.Position.X := loButtons.Width;
          if i=0 then
          begin
            btn.IsPressed := True;
            CurrentThread := Store.Threads[i].Key;
          end else
            btn.IsPressed := False;
          THideBusyAction.Post;
        end;
      finally
        loButtons.EndUpdate;
      end;
      Self.Invalidate;
    end
  );
end;

procedure TfrmMain.FormDestroy(Sender: TObject);
begin
  TFilesStore.Data.UnregisterForUpdates(Self);
  TOpenStore.Data.UnregisterForUpdates(Self);
end;

procedure TfrmMain.OnScroll(Sender: TObject);
begin
  Self.Invalidate;
end;

procedure TfrmMain.OpenFileChildCountNeeded(Sender: TObject; Path: TArray<Cardinal>; out LineNumber, Count: Cardinal);
var
  lst : TList<ILineGroupInfo>;
  idx : Cardinal;
  li : ILineGroupInfo;
begin
  idx := 0;
  lst := TOpenStore.Data.ListForThread(FCurrentThread);
  repeat
    li := lst.Items[Path[idx]];
    LineNumber := li.LineFrom;
    lst := li.Items;
    inc(idx);
  until idx >= Length(Path);
  Count := lst.Count;
end;

procedure TfrmMain.OpenFileTextNeeded(Sender: TObject; Path: TArray<Cardinal>; var LineNumber : Cardinal; var Text : TArray<String>; var BackgroundColor : TAlphaColor; var ChildCount : Cardinal );
var
  item : ILineGroupInfo;
begin
  item := PathToItem(FCurrentThread, Path);
  LineNumber := item.LineFrom;
  Text[0] := item.LineFrom.ToString;
  Text[1] := item.Duration.ToString;
  Text[2] := Item.Delta.ToString;
  Text[3] := item.Text;
  if Item.Level <> 'ENTER' then
    Text[4] := Item.Level
  else if Item.Items.Count > 0 then
    Text[4] := '('+Item.Items.Count.ToString+' items)'
  else
    Text[4] := '';
  DateTimeToString(Text[5], 'yyyy-mm-dd hh:nn:ss.zzz', Item.Timestamp);

  ChildCount := item.items.count;

  if Item.Level = 'DEBUG' then
    BackgroundColor := $99BCC7E8
  else if Item.Level = 'TRACE' then
    BackgroundColor := $99E7E8EA
  else if Item.Level = 'INFO' then
    BackgroundColor := $99F8F7E8
  else if Item.Level = 'ERROR' then
    BackgroundColor := $FFCC9C9C
  else if Item.Level = 'WARN' then
    BackgroundColor := $99F29F7F
  else if Item.Level = 'WARN' then
    BackgroundColor := $99F29F7F
  else if Item.Level = 'LICENSE' then
    BackgroundColor := $99C9E1C9
  else if Item.Level = 'FATAL' then
    BackgroundColor := $99FB0000
  else
    BackgroundColor := $99FFFFFF;
end;

function TfrmMain.PathToItem(ThreadID : UInt64; Path: TArray<Cardinal>): ILineGroupInfo;
var
  lst : TList<ILineGroupInfo>;
  idx : Cardinal;
  stack : TStack<Cardinal>;
begin
  Result := nil;

  idx := 0;
  lst := TOpenStore.Data.ListForThread(FCurrentThread);

  stack := TStack<Cardinal>.Create;
  try
    repeat
      if idx >= lst.Count then
      begin
        break;
      end;
      Result:= lst.Items[Path[idx]];
      lst := Result.Items;
      inc(idx);
    until idx >= Length(Path);
  finally
    stack.Free;
  end;

end;

procedure TfrmMain.RebuildTree;
begin
  TShowBusyAction.Post(Self);
  if FShowErrors then
    rrErrors.Fill.Kind := TBrushKind.Solid
  else
    rrErrors.Fill.Kind := TBrushKind.None;

  if FShowInfo then
    rrInfo.Fill.Kind := TBrushKind.Solid
  else
    rrInfo.Fill.Kind := TBrushKind.None;

  if FShowWarnings then
    rrWarnings.Fill.Kind := TBrushKind.Solid
  else
    rrWarnings.Fill.Kind := TBrushKind.None;

  if FShowTraceDebug then
    rrTraceDebug.Fill.Kind := TBrushKind.Solid
  else
    rrTraceDebug.Fill.Kind := TBrushKind.None;

  FOpenFile.RootCount := TOpenStore.Data.ListForThread(FCurrentThread).Count;
end;

procedure TfrmMain.SelectItem(Sender: TObject; Path: TArray<Cardinal>);
var
  lst : TList<ILineGroupInfo>;
  item : ILineGroupInfo;
  idx : Cardinal;
begin
  idx := 0;
  lst := TOpenStore.Data.ListForThread(FCurrentThread);
  repeat
    if Path[idx] >= lst.Count then
      exit;
    item := lst.Items[Path[idx]];
    lst := item.Items;
    inc(idx);
  until idx >= Length(Path);

  TSelectLogEntryAction.Post(FSelectedFile, Item);
  Self.Invalidate;
end;

procedure TfrmMain.SelectThread(Sender: TObject);
var
  ctrl : TControl;
begin
  for ctrl in loButtons.Controls do
    if ctrl is TButton then
    begin
      TButton(ctrl).IsPressed := ctrl = Sender;
      if ctrl = Sender then
        CurrentThread := TButton(ctrl).Tag;
    end;
end;

procedure TfrmMain.SetCurrentThread(const Value: UInt64);
begin
  FCurrentThread := Value;
  RebuildTree;
end;

procedure TfrmMain.SetSelectedFile(const Value: TFilerecord);
begin
  FSelectedFile := Value;
  txtCaption.Text := Value.FileName;
  TShowBusyAction.Post(Self);
  TViewLogFileAction.Post(Value.Path+Value.Filename);
  if tvFiles.width > 0 then
    txtMenuClick(nil);
end;

procedure TfrmMain.tvFilesChange(Sender: TObject);
begin
  if tvFiles.Selected <> nil then
  begin
    if (tvFiles.Selected is TFileTreeViewItem) then
    begin
      SelectedFile := TFileTreeViewItem(tvFiles.Selected).item;
    end;
  end;
end;

procedure TfrmMain.txtCaptionClick(Sender: TObject);
begin
  txtOpenClick(Sender);
end;

procedure TfrmMain.txtErrorsClick(Sender: TObject);
begin
  FShowErrors := not FShowErrors;
  seErrors.Enabled := not FShowErrors;
  RebuildTree;
end;

procedure TfrmMain.txtInfoClick(Sender: TObject);
begin
  FShowInfo := not FShowInfo;
  seInfo.Enabled := not FShowInfo;
  RebuildTree;
end;

procedure TfrmMain.txtMenuClick(Sender: TObject);
begin
  if tvFiles.Width = 0 then
  begin
    TAnimator.AnimateFloat(tvFiles, 'Width', FFilesWidth);
    splLeft.Visible := True;
    seMenu.Enabled := False;
  end else
  begin
    FFilesWidth := tvFiles.Width;
    TAnimator.AnimateFloat(tvFiles, 'Width', 0);
    splLeft.Visible := False;
    seMenu.Enabled := True;
  end;
  Self.Invalidate;
end;

procedure TfrmMain.txtOpenClick(Sender: TObject);
begin
  if dlgOpen.Execute then
  begin
    for var sFile in dlgOpen.Files do
    begin
      TOpenLogFileAction.Post(sFile);
    end;
  end;
end;

procedure TfrmMain.txtTraceDebugClick(Sender: TObject);
begin
  FShowTraceDebug := not FShowTraceDebug;
  seTraceDebug.Enabled := not FShowTraceDebug;
  RebuildTree;
end;

procedure TfrmMain.txtWarningsClick(Sender: TObject);
begin
  FShowWarnings := not FShowWarnings;
  seWarnings.Enabled := not FShowWarnings;
  RebuildTree;
end;

procedure TfrmMain.UpdateAppFiles(Files: TDictionary<string, TList<TFileRecord>>);
var
  bFound : boolean;
begin
  var ary := Files.ToArray;
  for var a in ary do
  begin
    bFound := False;
    for var i := 0 to tviApplications.Count-1 do
    begin
      if tviApplications.Items[i].Text = a.Key then
      begin
        bFound := True;
        FillFromParent(tviApplications.Items[i], a.Value);
        break;
      end;
    end;
    if not bFound then
    begin
      var tvi := TTreeViewItem.Create(tvFiles);
      tvi.Parent := tviApplications;
      tvi.Text := a.Key;
      FillFromParent(tvi, a.Value);
    end;
  end;
end;

procedure TfrmMain.UpdateDownloads(Files: TList<TFileRecord>);
  function FindParent(Text : string) : TTreeViewItem;
  var
    i : integer;
  begin
    Result := nil;
    for i := 0 to tviDownloads.Count-1 do
    begin
      if tviDownloads.Items[i].Text = Text then
      begin
        Result := tviDownloads.Items[i];
        break;
      end;
    end;
    if not Assigned(Result) then
    begin
      Result := TTreeViewItem.Create(tvFiles);
      Result.Text := Text;
      Result.Parent := tviDownloads;
    end;
  end;
  procedure AddFile(Parent : TTreeViewItem; FileInfo : TFileRecord);
  begin
    var tvi := TFileTreeViewItem.Create(tvFiles);
    tvi.Item := FileInfo;
    tvi.Parent := Parent;
  end;
begin
  tviDownloads.BeginUpdate;
  try
    while tviDownloads.Count > 0 do
    begin
      var tvi := tviDownloads.Items[0];
      tvi.Parent := nil;
      tvi.Free;
    end;
    for var fi in Files do
    begin
      if fi.RelativeFilePath = fi.FileName then
        AddFile(tviDownloads, fi)
      else
        AddFile(FindParent(fi.RelativeFilePath.Replace(fi.FileName,'')), fi);
    end;
  finally
    tviDownloads.EndUpdate;
  end;
end;

procedure TfrmMain.UpdateFiles(Files: TList<TFileRecord>);
begin
  FillFromParent(tviOther, Files);
end;

{ TEntryTreeViewItem }

{constructor TEntryTreeViewItem.Create(AOwner: TComponent);
begin
  inherited;
  FItem := nil;
  FDuration := TText.Create(Self);
  FDelta := TText.Create(Self);
  FLevel := TText.Create(Self);
  FTimestamp := TText.Create(Self);
  FID := TText.Create(Self);
  FMsg := TText.Create(Self);
  FBack := TRectangle.Create(Self);

  BeginUpdate;
  try
    FBack.Parent := Self;
    FBack.Stroke.Kind := TBrushKind.None;
    FBack.Fill.Color := TAlphaColorRec.White;
    FBack.Align := TAlignLayout.Client;
    FBack.Margins.Left := 30;
    FBack.Padding.Top := 4;
    FBack.HitTest := False;
    FBack.Locked := True;

    FID.Parent := FBack;
    FID.TextSettings.HorzAlign := TextAlign.taLeading;
    FID.TextSettings.VertAlign := TTextAlign.Leading;
    FID.TextSettings.font.Family := 'Courier New';
    FID.TextSettings.WordWrap := False;
    FID.Align := TAlignLayout.Left;
    FID.Width := 75;
    FID.HitTest := False;
    FID.Locked := True;

    FDelta.Parent := FBack;
    FDelta.Align := TAlignLayout.Left;
    FDelta.TextSettings.VertAlign := TTextAlign.Leading;
    FDelta.TextSettings.HorzAlign := TTextAlign.taLeading;
    FDelta.TextSettings.font.Family := 'Courier New';
    FDelta.TextSettings.WordWrap := False;
    FDelta.Width := 75;
    FDelta.HitTest := False;
    FDelta.Locked := True;

    FDuration.Parent := FBack;
    FDuration.Align := TAlignLayout.Left;
    FDuration.TextSettings.font.Family := 'Courier New';
    FDuration.TextSettings.VertAlign := TTextAlign.Leading;
    FDuration.TextSettings.HorzAlign := TextAlign.taLeading;
    FDuration.TextSettings.WordWrap := False;
    FDuration.Width := 75;
    FDuration.HitTest := False;
    FDuration.Locked := True;

    FLevel.Parent := FBack;
    FLevel.Align := TAlignLayout.Right;
    FLevel.TextSettings.font.Family := 'Courier New';
    FLevel.TextSettings.VertAlign := TTextAlign.Leading;
    FLevel.TextSettings.HorzAlign := TextAlign.taLeading;
    FLevel.TextSettings.WordWrap := False;
    FLevel.Width := 75;
    FLevel.HitTest := False;
    FLevel.Locked := True;

    FTimestamp.Parent := FBack;
    FTimestamp.Align := TAlignLayout.Right;
    FTimestamp.TextSettings.font.Family := 'Courier New';
    FTimestamp.TextSettings.VertAlign := TTextAlign.Leading;
    FTimestamp.TextSettings.HorzAlign := TextAlign.taLeading;
    FTimestamp.TextSettings.WordWrap := False;
    FTimestamp.Width := 175;
    FTimestamp.HitTest := False;
    FTimestamp.Locked := True;
    FTimestamp.Position.X := 0;

    FMsg.Parent := FBack;
    FMsg.Align := TAlignLayout.Client;
    FMsg.TextSettings.font.Family := 'Courier New';
    FMsg.TextSettings.WordWrap := False;
    FMsg.TextSettings.HorzAlign := TextAlign.taLeading;
    FMsg.TextSettings.VertAlign := TTextAlign.Leading;
    FMsg.TextSettings.WordWrap := False;
    FMsg.HitTest := False;
    FMsg.Locked := True;
  finally
    EndUpdate;
  end;
end;

destructor TEntryTreeViewItem.Destroy;
begin
  FDuration.Free;
  FDelta.Free;
  FLevel.Free;
  FID.Free;
  FMsg.Free;
  inherited;
end;

procedure TEntryTreeViewItem.SetItem(const Value: ILineGroupInfo);
begin
  FItem := Value;

  if FItem.Level = 'DEBUG' then
    FBack.Fill.Color := $99BCC7E8
  else if FItem.Level = 'TRACE' then
    FBack.Fill.Color := $99E7E8EA
  else if FItem.Level = 'INFO' then
    FBack.Fill.Color := $99F8F7E8
  else if FItem.Level = 'ERROR' then
    FBack.Fill.Color := $FFCC9C9C
  else if FItem.Level = 'WARN' then
    FBack.Fill.Color := $99F29F7F
  else if FItem.Level = 'WARN' then
    FBack.Fill.Color := $99F29F7F
  else if FItem.Level = 'LICENSE' then
    FBack.Fill.Color := $99C9E1C9
  else if FItem.Level = 'FATAL' then
    FBack.Fill.Color := $99FB0000
  else
    FBack.Fill.Color := $99FFFFFF;


  FDuration.Text := FItem.Duration.ToString;
  FDelta.Text := FItem.Delta.ToString;
  FID.Text := FItem.LineFrom.ToString;
  if FItem.LineFrom <> FItem.LineTo then
    FID.Text := FID.Text+'-'+FItem.LineTo.ToString;
  FMsg.Text := FItem.Text;
  Hint := FItem.Text;
  ShowHint := True;
  if FItem.Level <> 'ENTER' then
    FLevel.Text := FItem.Level
  else if FItem.Items.Count > 0 then
    FLevel.Text := '('+FItem.Items.Count.ToString+' items)'
  else
    FLevel.Text := '';
  FTimestamp.Text := DateToISO8601(FItem.Timestamp, False);
end;
 }
{ TFileTreeViewItem }

constructor TFileTreeViewItem.Create(AOwner: TComponent);
begin
  inherited;
  FFileSize := TText.Create(Self);
end;

destructor TFileTreeViewItem.Destroy;
begin

  inherited;
end;

procedure TFileTreeViewItem.SetItem(const Value: TFileRecord);
  function FileSizeToText(Size : UInt64) : string;
  var
    kb, mb, gb : Double;
  begin
    kb := Size / 1024;
    mb := kb / 1000;
    gb := mb / 1000;
    if gb > 1 then
      Result := (Round(gb * 100) / 100).ToString+' GB'
    else if mb > 1 then
      Result := (Round(mb * 100) / 100).ToString+' MB'
    else if kb > 1 then
      Result := (Round(kb * 100) / 100).ToString+' KB'
    else
      Result := Size.ToString+' Bytes';
  end;
begin
  FItem := Value;
  FFileSize.Parent := Self;
  FFileSize.Align := TAlignLayout.Right;
  FFileSize.Text := FileSizeToText(Item.Size);
  FFileSize.Width := 75;
  FFileSize.HitTest := False;
  FFileSize.Locked := False;
  Text := Value.FileName;
end;

end.
