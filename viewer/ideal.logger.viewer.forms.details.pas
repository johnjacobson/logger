unit ideal.logger.viewer.forms.details;

interface

uses
  System.SysUtils,
  System.Types,
  System.UITypes,
  System.Classes,
  System.Variants,
  ideal.logger.viewer.types,
  FMX.Types,
  FMX.Controls,
  FMX.Forms,
  FMX.Graphics,
  FMX.Dialogs,
  FMX.Memo.Types,
  FMX.Controls.Presentation,
  FMX.ScrollBox,
  FMX.Memo, FMX.TabControl;

type
  TfrmDetails = class(TForm)
    txtDetails: TMemo;
    TabControl1: TTabControl;
    TabItem1: TTabItem;
    TabItem2: TTabItem;
    txtSource: TMemo;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    FFilepath: string;
    FLine: ILineGroupInfo;
    function GetDetails: string;
    procedure SetDetails(const Value: string);
    procedure SetFilepath(const Value: string);
    procedure SetLine(const Value: ILineGroupInfo);
    function GetSource: string;
    procedure SetSource(const Value: string);
    { Private declarations }
  public
    property Filepath : string read FFilepath write SetFilepath;
    property Line : ILineGroupInfo read FLine write SetLine;
    property Details : string read GetDetails write SetDetails;
    property Source : string read GetSource write SetSource;
  end;

implementation

uses
  ideal.logger.viewer.actions,
  cocinasync.flux.view.fmx;

{$R *.fmx}

procedure TfrmDetails.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := TCloseAction.caFree;
end;

function TfrmDetails.GetDetails: string;
begin
  Result := txtDetails.Text;
end;

function TfrmDetails.GetSource: string;
begin
  result := txtSource.Text;
end;

procedure TfrmDetails.SetDetails(const Value: string);
begin
  txtDetails.Text := Value;
end;

procedure TfrmDetails.SetFilepath(const Value: string);
begin
  FFilepath := Value;
end;

procedure TfrmDetails.SetLine(const Value: ILineGroupInfo);
begin
  FLine := Value;
end;

procedure TfrmDetails.SetSource(const Value: string);
begin
  txtSource.Text := Value;
end;

initialization

  TViews.&On<TShowDetailAction>.CreateAndShow(TfrmDetails,
    procedure(Form : TCustomForm; Action : TShowDetailAction)
    begin
      TfrmDetails(Form).Filepath := Action.Filepath;
      TfrmDetails(Form).Line := Action.Line;
      TfrmDetails(Form).Details := Action.Details;
      TfrmDetails(Form).Source := Action.Source;
    end
  );

finalization

end.
